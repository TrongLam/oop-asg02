package uet.oop.bomberman.entities.bomb;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.enemy.Enemy;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.entities.character.Character;

/**
 * Class cho Flame cua game hoat dong 
 * @author Nguyen Trong Lam & Pham Thanh Hung
 * @version 1.0
 * @Since 12/2/2018
 * 
 */
public class Flame extends Entity {

	protected Board _board;
	protected int _direction;
	private int _radius;
	protected int xOrigin, yOrigin;
	protected FlameSegment[] _flameSegments = new FlameSegment[0];

	/**
	 *
	 * @param x ho�nh ?? b?t ??u c?a Flame
	 * @param y tung ?? b?t ??u c?a Flame
	 * @param direction l� h??ng c?a Flame
	 * @param radius ?? d�i c?c ??i c?a Flame
	 */
	public Flame(int x, int y, int direction, int radius, Board board) {
		xOrigin = x;
		yOrigin = y;
		_x = x;
		_y = y;
		_direction = direction;
		_radius = radius;
		_board = board;
                _flameSegments = new FlameSegment[calculatePermitedDistance()];
		createFlameSegments();
                
	}

	/**
	 * T?o c�c FlameSegment, m?i segment ?ng m?t ??n v? ?? d�i
	 */
	private void createFlameSegments() {
		/**
		 * bi?n last d�ng ?? ?�nh d?u cho segment cu?i c�ng
		 */
               	boolean last = false;
		/**
		 * t�nh to�n ?? d�i Flame, t??ng ?ng v?i s? l??ng segment
		 */
		int x = (int) _x;
		int y = (int) _y;
                // TODO: t?o c�c segment d??i ?�y
               // System.out.println(_flameSegments.length);
               // System.out.println("end");
		for (int i = 0; i < _flameSegments.length; i++) {
			if( i == _flameSegments.length -1 ){
                            last=true;
                        }
                        else{
                            last=false;
                        }
			
			switch (_direction) {
				case 0: y--; break;
				case 1: x++; break;
				case 2: y++; break;
				case 3: x--; break;
			}
			_flameSegments[i] = new FlameSegment(x, y, _direction, last);
		}

		
		

		
	}

	/**
	 * T�nh to�n ?? d�i c?a Flame, n?u g?p v?t c?n l� Brick/Wall, ?? d�i s? b? c?t ng?n
	 * @return
	 */
	private int calculatePermitedDistance() {
		// TODO: th?c hi?n t�nh to�n ?? d�i c?a Flame
                int radius = 0;// tim xung quanh xem co bi can tro khong
		int x = (int)_x;
		int y = (int)_y;
             
		while(radius < _radius) {
			if(_direction == 0) y--;
			if(_direction == 1) x++;
			if(_direction == 2) y++;
			if(_direction == 3) x--;
			
			Entity a = _board.getEntity(x, y, null);
			
			if(a instanceof Character) ++radius; //explosion has to be below the mob
                       
			if(a.collide(this) == false) //cannot pass thru
				break;
			
			++radius;
		}
		return radius;
		//return 10;
	}
	
	public FlameSegment flameSegmentAt(int x, int y) {
		for (int i = 0; i < _flameSegments.length; i++) {
			if(_flameSegments[i].getX() == x && _flameSegments[i].getY() == y)
				return _flameSegments[i];
		}
		return null;
	}

	@Override
	public void update() {}
	
	@Override
	public void render(Screen screen) {
		for (int i = 0; i < _flameSegments.length; i++) {
			_flameSegments[i].render(screen);
		}
	}

	@Override
	public boolean collide(Entity e) {
		// TODO: x? l� va ch?m v?i Bomber, Enemy. Ch� � ??i t??ng n�y c� v? tr� ch�nh l� v? tr� c?a Bomb ?� n?
		if(e instanceof Character){
                    return false;
                }
                return true;
	}
}
