package uet.oop.bomberman.entities.tile.item;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.graphics.Sprite;
/**
 * Class cho BombItem cua game hoat dong 
 * @author Nguyen Trong Lam & Pham Thanh Hung
 * @version 1.0
 * @Since 12/2/2018
 * 
 */
public class BombItem extends Item {

	public BombItem(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}
        /**
         * La ham kiem tra va tram cua Item voi Enity
         * @param e Enity truyen vao
         * @return boolean la kieu tra ve
         */
	public boolean collide(Entity e) {
		
		if(e instanceof Bomber) {
 
                    System.out.println(Game.getBombRate());
                    String filename = "WARP1.wav";
                    ContinuousAudioDataStream loop = null;
                    InputStream in = null;
                    try {
                        in = new FileInputStream(filename);
                    } catch (FileNotFoundException ex) {
                        System.out.println("File not found");
                    }
                    try {
                        AudioStream s = new AudioStream(in);
                        AudioData MD;
                        AudioPlayer.player.start(s);
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
			remove();
                       Game.addBombRate(1);

		}
		
		return false;
	}
	
	
	
	
	


}
