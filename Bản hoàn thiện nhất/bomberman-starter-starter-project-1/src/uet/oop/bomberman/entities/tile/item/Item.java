package uet.oop.bomberman.entities.tile.item;

import uet.oop.bomberman.entities.tile.Tile;
import uet.oop.bomberman.graphics.Sprite;
/**
 * Class cho Item cua game hoat dong 
 * @author Nguyen Trong Lam & Pham Thanh Hung
 * @version 1.0
 * @Since 12/2/2018
 * 
 */
public abstract class Item extends Tile {
       
	public Item(int x, int y, Sprite sprite) {
		super(x, y, sprite);
               
	}
        

	
}
