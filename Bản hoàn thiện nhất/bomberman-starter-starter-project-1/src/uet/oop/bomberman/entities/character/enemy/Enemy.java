package uet.oop.bomberman.entities.character.enemy;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.Message;
import uet.oop.bomberman.entities.bomb.Flame;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.Character;
import uet.oop.bomberman.entities.character.enemy.ai.AI;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.Coordinates;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.UUID;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;
/**
 * Class cho Enemy cua game hoat dong 
 * @author Nguyen Trong Lam & Pham Thanh Hung
 * @version 1.0
 * @Since 12/2/2018
 * 
 */
public abstract class Enemy extends Character {

	protected int _points;
	
	protected double _speed;
	protected AI _ai;

	protected final double MAX_STEPS;
	protected final double rest;
	protected double _steps;
	
	protected int _finalAnimation = 30;
	protected Sprite _deadSprite;
	
	public Enemy(int x, int y, Board board, Sprite dead, double speed, int points) {
		super(x, y, board);
		
		_points = points;
		_speed = speed;
		
		MAX_STEPS = Game.TILES_SIZE / _speed;
		rest = (MAX_STEPS - (int) MAX_STEPS) / MAX_STEPS;
		_steps = MAX_STEPS;
		
		_timeAfter = 20;
		_deadSprite = dead;
	}
	
	@Override
	public void update() {
		animate();
		
		if(!_alive) {
			afterKill();
			return;
		}
		
		if(_alive)
			calculateMove();
	}
	
	@Override
	public void render(Screen screen) {
		
		if(_alive)
			chooseSprite();
		else {
			if(_timeAfter > 0) {
				_sprite = _deadSprite;
				_animate = 0;
			} else {
				_sprite = Sprite.movingSprite(Sprite.mob_dead1, Sprite.mob_dead2, Sprite.mob_dead3, _animate, 60);
			}
				
		}
			
		screen.renderEntity((int)_x, (int)_y - _sprite.SIZE, this);
	}
	
	@Override
        /**
         * Tinh toan di chuyen cua enemy
         */
	public void calculateMove() {
		// TODO: T�nh to�n h??ng ?i v� di chuy?n Enemy theo _ai v� c?p nh?t gi� tr? cho _direction
		// TODO: s? d?ng canMove() ?? ki?m tra xem c� th? di chuy?n t?i ?i?m ?� t�nh to�n hay kh�ng
		// TODO: s? d?ng move() ?? di chuy?n
		// TODO: nh? c?p nh?t l?i gi� tr? c? _moving khi thay ??i tr?ng th�i di chuy?n
               int xa = 0, ya = 0;
		if(_steps <= 0){
			_direction = _ai.calculateDirection();
			_steps = MAX_STEPS;
		}
			
		if(_direction == 0) ya--; 
		if(_direction == 2) ya++;
		if(_direction == 3) xa--;
		if(_direction == 1) xa++;
		
		if(canMove(xa, ya)) {
			_steps -= 1 + rest;
			move(xa * _speed, ya * _speed);
			_moving = true;
		} else {
			_steps = 0;
			_moving = false;
		}
                
	}
	
	@Override
        /**
         * Di chuyen enemy
         */
	public void move(double xa, double ya) {
		if(!_alive) return;
		_y += ya;
		_x += xa;
	}
	
	@Override
        /**
         * Kiem tra xem Enemy co the di chuyen dc khong 
         */
	public boolean canMove(double x, double y) {
		// TODO: ki?m tra c� ??i t??ng t?i v? tr� chu?n b? di chuy?n ??n v� c� th? di chuy?n t?i ?� hay kh�ng
            Entity e[] = new Entity[4];
            e[0] = _board.getEntity(Coordinates.pixelToTile(_x + x ), Coordinates.pixelToTile(_y + y) - 1, this);//kiem tra sau lung enemy
            e[1] = _board.getEntity(Coordinates.pixelToTile(_x + _sprite.getRealWidth() + x-1 ), Coordinates.pixelToTile(_y + y) - 1, this);// kiem tra dau ben phai cua enemy
            e[2] = _board.getEntity(Coordinates.pixelToTile(_x + _sprite.getRealWidth() + x -1), Coordinates.pixelToTile(_y + y - 1), this);// kiem tra chan ben phai cua enemy
            e[3] = _board.getEntity(Coordinates.pixelToTile(_x + x ), Coordinates.pixelToTile(_y + y - 1), this);// kiem tra duoi chan cua enemy
        for(int i=0;i<4;i++){
            if(!e[i].collide(this)){
                return false;
            }
        }
        return true;
		
	}

	@Override
        /**
         * kiem tra va tram cua enemy
         */
	public boolean collide(Entity e) {
		// TODO: x? l� va ch?m v?i Flame
		// TODO: x? l� va ch?m v?i Bomber
              if (e instanceof Flame) {
                String filename = "KURUKURU_11K.wav";
            ContinuousAudioDataStream loop = null;
            InputStream in = null;
            try {
                in = new FileInputStream(filename);
            } catch (FileNotFoundException ex) {
                System.out.println("File not found");
            }
            try {
                AudioStream s = new AudioStream(in);
                AudioData MD;
                AudioPlayer.player.start(s);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
                    kill();
                    return false;
                }
		return true;
	}
	
	@Override
	public void kill() {
		if(!_alive) return;
		_alive = false;
		
		_board.addPoints(_points);

		Message msg = new Message("+" + _points, getXMessage(), getYMessage(), 2, Color.white, 14);
		_board.addMessage(msg);
	}
	
	
	@Override
	protected void afterKill() {
		if(_timeAfter > 0) --_timeAfter;
		else {
			if(_finalAnimation > 0) --_finalAnimation;
			else
				remove();
		}
	}
	
	protected abstract void chooseSprite();
}
