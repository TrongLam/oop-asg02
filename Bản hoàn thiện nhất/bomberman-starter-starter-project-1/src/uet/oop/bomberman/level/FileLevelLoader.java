package uet.oop.bomberman.level;

import static java.awt.PageAttributes.MediaType.C;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;
import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.LayeredEntity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.enemy.Balloon;
import uet.oop.bomberman.entities.tile.Grass;
import uet.oop.bomberman.entities.tile.Portal;
import uet.oop.bomberman.entities.tile.Wall;
import uet.oop.bomberman.entities.tile.destroyable.Brick;
import uet.oop.bomberman.entities.tile.item.BombItem;
import uet.oop.bomberman.entities.tile.item.FlameItem;
import uet.oop.bomberman.entities.tile.item.SpeedItem;
import uet.oop.bomberman.exceptions.LoadLevelException;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
/**
 * Class cho Map cua game hoat dong 
 * @author Nguyen Trong Lam & Pham Thanh Hung
 * @version 1.0
 * @Since 12/2/2018
 * 
 */
public class FileLevelLoader extends LevelLoader {

	/**
	 * Ma tr?n ch?a th�ng tin b?n ??, m?i ph?n t? l?u gi� tr? k� t? ??c ???c
	 * t? ma tr?n b?n ?? trong t?p c?u h�nh
	 */
	private static char[][] _map;
	
	public FileLevelLoader(Board board, int level) throws LoadLevelException {
		super(board, level);
	}
	
	@Override
	public void loadLevel(int level) throws LoadLevelException {
		// TODO: ??c d? li?u t? t?p c?u h�nh /levels/Level{level}.txt
		// TODO: c?p nh?t c�c gi� tr? ??c ???c v�o _width, _height, _level, _map
                
                // Doc tu File leve1.txt
                String text ="";
                try{
                FileReader file =new FileReader("res\\levels\\Level1.txt"); //Doc map trong File Text
                BufferedReader br = new BufferedReader(file);// Truyen File
                String line=br.readLine();// ??c 
                    while(line!=null){
                        text+=line;
                        text+="\n";
                            line=br.readLine();// doc tung dong
                    }
                file.close();
                br.close();
                }
                catch (IOException ex) {
                   throw new LoadLevelException();
            }
                
                // Lay du Lieu tu  text vua doc
                
                Scanner scan=new Scanner(text);
                _level=scan.nextInt();// Cap nhat level
                _height=scan.nextInt();// Cap nhat height
                _width=scan.nextInt();// Cap nhat width
                String temptext = scan.nextLine();
                _map=new char [_height][_width];
                try{
                for (int i = 0; i < _height; i++) {
                    temptext = scan.nextLine();
                    for (int j = 0; j < _width; j++) {
                        _map[i][j] = temptext.charAt(j);
                    }
                }
                }
                catch(Exception e){
                    System.out.println("Loi roi");
                }
                
             // Bat am thanh nen cua game
             
            String filename = "03_Stage Theme.wav";
            ContinuousAudioDataStream loop = null;
            InputStream in = null;
            try {
                in = new FileInputStream(filename);
            } catch (FileNotFoundException ex) {
                System.out.println("File not found");
            }
            try {
                AudioStream s = new AudioStream(in);
                AudioData MD;
                AudioPlayer.player.start(s);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
                
	}

	@Override
	public void createEntities() {
		// TODO: t?o c�c Entity c?a m�n ch?i
		// TODO: sau khi t?o xong, g?i _board.addEntity() ?? th�m Entity v�o game

		// TODO: ph?n code m?u ? d??i ?? h??ng d?n c�ch th�m c�c lo?i Entity v�o game
		// TODO: h�y x�a n� khi ho�n th�nh ch?c n?ng load m�n ch?i t? t?p c?u h�nh
		// th�m Wall
//		for (int x = 0; x < _height; x++) {
//			for (int y = 0; y < _width; y++) {
//				int pos = y+ x * _width;
//				
//				_board.addEntity(pos, new Grass(x, y, Sprite.grass));
//			}
//		}
 // Huong Dan
               
                for (int i = 0; i < _height; i++) {
			for (int j = 0; j < _width; j++) {
				createLevelEntities(_map[i][j],j,i);
                                
                }
				
			}
                
               
// Huong Dan
//		// th�m Bomber
//		int xBomber = 1, yBomber = 1;
//		_board.addCharacter( new Bomber(Coordinates.tileToPixel(xBomber), Coordinates.tileToPixel(yBomber) + Game.TILES_SIZE, _board) );
//		Screen.setOffset(0, 0);
//		_board.addEntity(xBomber + yBomber * _width, new Grass(xBomber, yBomber, Sprite.grass));
//
//		// th�m Enemy
//		int xE = 2, yE = 1;
//		_board.addCharacter( new Balloon(Coordinates.tileToPixel(xE), Coordinates.tileToPixel(yE) + Game.TILES_SIZE, _board));
//		_board.addEntity(xE + yE * _width, new Grass(xE, yE, Sprite.grass));
//
//		// th�m Brick
//		int xB = 3, yB = 1;
//		_board.addEntity(xB + yB * _width,
//				new LayeredEntity(xB, yB,
//					new Grass(xB, yB, Sprite.grass),
//					new Brick(xB, yB, Sprite.brick)
//				)
//		);
//
//		// th�m Item k�m Brick che ph? ? tr�n
//		int xI = 1, yI = 2;
//		_board.addEntity(xI + yI * _width,
//				new LayeredEntity(xI, yI,
//					new Grass(xI ,yI, Sprite.grass),
//					new SpeedItem(xI, yI, Sprite.powerup_flames),
//					new Brick(xI, yI, Sprite.brick)
//				)
//		);
	}
       public  void createLevelEntities(char c,int x,int y){
           int pos = x + y * _width;// Tinh toan vi tri trong board
		switch(c) { // TODO: minimize this method
			case '#': //Wall
				_board.addEntity(pos, new Wall(x, y, Sprite.wall));  
				break;
			case 'b': // BombTitem
				_board.addEntity(x + y * _width,
				new LayeredEntity(x, y,
					new Grass(x ,y, Sprite.grass),
					new BombItem(x, y, Sprite.powerup_bombs),
					new Brick(x, y, Sprite.brick)
				)
		);
				break;
			case 's':// SpeedItem
				_board.addEntity(x + y * _width,
				new LayeredEntity(x, y,
					new Grass(x ,y, Sprite.grass),
					new SpeedItem(x, y, Sprite.powerup_speed),
					new Brick(x, y, Sprite.brick)
				)
		);
				break;
			case 'f': // FlameItem
				_board.addEntity(x + y * _width,
				new LayeredEntity(x, y,
					new Grass(x ,y, Sprite.grass),
					new FlameItem(x, y, Sprite.powerup_flames),
					new Brick(x, y, Sprite.brick)
				)
		);
				break;
			case '*': // brick
				_board.addEntity(pos, new LayeredEntity(x, y, 
						new Grass(x ,y, Sprite.grass), 
						new Brick(x ,y, Sprite.brick)) );
				break;
			case 'x': // Portal
				_board.addEntity(pos, new LayeredEntity(x, y, 
						new Grass(x ,y, Sprite.grass), 
						new Portal(x ,y,_board, Sprite.portal), 
						new Brick(x ,y, Sprite.brick)) );
				break;
			case ' ': // Grass
				_board.addEntity(pos, new Grass(x, y, Sprite.grass) );
				break;
			case 'p': // Boomber
				_board.addCharacter( new Bomber(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board) );
				Screen.setOffset(0, 0);
				
				_board.addEntity(pos, new Grass(x, y, Sprite.grass) );
				break;
			//Enemies
			case '1': // Balloon
				_board.addCharacter( new Balloon(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board));
                                _board.addEntity(x + y * _width, new Grass(x, y, Sprite.grass));
				break;
			
			default: 
				_board.addEntity(pos, new Grass(x, y, Sprite.grass) );
				break;
			}
       }
}
  

//}
