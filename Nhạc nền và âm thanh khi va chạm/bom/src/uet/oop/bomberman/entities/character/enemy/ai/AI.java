package uet.oop.bomberman.entities.character.enemy.ai;

import java.util.Random;
/**
 * Class cho AI cua game hoat dong 
 * @author Nguyen Trong Lam & Pham Thanh Hung
 * @version 1.0
 * @Since 12/2/2018
 * 
 */
public abstract class AI {
	
	protected Random random = new Random();

	/**
	 * Thuật toán tìm đư�?ng đi
	 * @return hướng đi xuống/phải/trái/lên tương ứng với các giá trị 0/1/2/3
	 */
	public abstract int calculateDirection();
}
