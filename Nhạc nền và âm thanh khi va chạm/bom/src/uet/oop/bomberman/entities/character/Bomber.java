package uet.oop.bomberman.entities.character;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.bomb.Bomb;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.input.Keyboard;

import java.util.Iterator;
import java.util.List;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;
import uet.oop.bomberman.entities.bomb.Flame;
import uet.oop.bomberman.entities.character.enemy.Enemy;
import uet.oop.bomberman.entities.tile.item.Item;
import uet.oop.bomberman.level.Coordinates;
/**
 * Class cho Bomber cua game hoat dong 
 * @author Nguyen Trong Lam & Pham Thanh Hung
 * @version 1.0
 * @Since 12/2/2018
 * 
 */
public class Bomber extends Character {

    private List<Bomb> _bombs;
    protected Keyboard _input;
    public static List<Item> _Item = new ArrayList<Item>();
    /**
     * n?u gi� tr? n�y < 0 th� cho ph�p ??t ??i t??ng Bomb ti?p theo,
     * c? m?i l?n ??t 1 Bomb m?i, gi� tr? n�y s? ???c reset v? 0 v� gi?m d?n trong m?i l?n update()
     */
    protected int _timeBetweenPutBombs = 0;

    public Bomber(int x, int y, Board board) {
        super(x, y, board);
        _bombs = _board.getBombs();
        _input = _board.getInput();
        _sprite = Sprite.player_right;
    }

    @Override
    public void update() {
        clearBombs();
        if (!_alive) {
            afterKill();
            return;
        }

        if (_timeBetweenPutBombs < -7500) _timeBetweenPutBombs = 0;
        else _timeBetweenPutBombs--;

        animate();

        calculateMove();

        detectPlaceBomb();
    }

    @Override
    public void render(Screen screen) {
        calculateXOffset();

        if (_alive)
            chooseSprite();
        else
            _sprite = Sprite.player_dead1;

        screen.renderEntity((int) _x, (int) _y - _sprite.SIZE, this);
    }

    public void calculateXOffset() {
        int xScroll = Screen.calculateXOffset(_board, this);
        Screen.setOffset(xScroll, 0);
    }

    /**
     * Ki?m tra xem c� ??t ???c bom hay kh�ng? n?u c� th� ??t bom t?i v? tr� hi?n t?i c?a Bomber
     */
    private void detectPlaceBomb() {
        // TODO: ki?m tra xem ph�m ?i?u khi?n ??t bom c� ???c g� v� gi� tr? _timeBetweenPutBombs, Game.getBombRate() c� th?a m�n hay kh�ng
        // TODO:  Game.getBombRate() s? tr? v? s? l??ng bom c� th? ??t li�n ti?p t?i th?i ?i?m hi?n t?i
        // TODO: _timeBetweenPutBombs d�ng ?? ng?n ch?n Bomber ??t 2 Bomb c�ng t?i 1 v? tr� trong 1 kho?ng th?i gian qu� ng?n
        // TODO: n?u 3 ?i?u ki?n tr�n th?a m�n th� th?c hi?n ??t bom b?ng placeBomb()
        // TODO: sau khi ??t, nh? gi?m s? l??ng Bomb Rate v� reset _timeBetweenPutBombs v? 0
        if(_input.space && Game.getBombRate() > 0 && _timeBetweenPutBombs < 0) {
			
			int xt = Coordinates.pixelToTile(_x+_sprite.getSize()/2);
			int yt = Coordinates.pixelToTile( (_y +_sprite.getSize()/2))-1; //subtract half player height and minus 1 y position
			
			placeBomb(xt,yt);
			Game.addBombRate(-1);
			
			_timeBetweenPutBombs = 30;
		}
    }
/**
 * Dat vi tri boomber
 * @param x la tham so truyen vao
 * @param y la tham so truyen vao
 */
    protected void placeBomb(int x, int y) {
        // TODO: th?c hi?n t?o ??i t??ng bom, ??t v�o v? tr� (x, y)
        Bomb b = new Bomb(x, y, _board);
       _board.addBomb(b);
    }

    private void clearBombs() {
        Iterator<Bomb> bs = _bombs.iterator();

        Bomb b;
        while (bs.hasNext()) {
            b = bs.next();
            if (b.isRemoved()) {
                bs.remove();
                Game.addBombRate(1);
            }
        }

    }

    @Override
    public void kill() {
        if (!_alive) return;
        _alive = false;
    }

    @Override
    protected void afterKill() {
        if (_timeAfter > 0) --_timeAfter;
        else {
            _board.endGame();
        }
    }

    @Override
    /**
     * Tinh toan vi tri di tiep theo nhap tu ban phim
     */
    protected void calculateMove() {
        // TODO: x? l� nh?n t�n hi?u ?i?u khi?n h??ng ?i t? _input v� g?i move() ?? th?c hi?n di chuy?n
        // TODO: nh? c?p nh?t l?i gi� tr? c? _moving khi thay ??i tr?ng th�i di chuy?n
        int xa = 0, ya = 0;
		if(_input.up) ya--;
		if(_input.down) ya++;
		if(_input.left) xa--;
		if(_input.right) xa++;
		
		if(xa != 0 || ya != 0)  {
			move(xa * Game.getBomberSpeed(), ya * Game.getBomberSpeed());
			_moving = true;
		} else {
			_moving = false;
		}
    }

    @Override
   /**
    * Kiem tra xem Boomber co the di den vi tri nhap tu ban phim 
    * x la tham so duoc truyen vao
    * y la tham so duoc truyen vao
    */
    public boolean canMove(double x, double y) {
        // TODO: ki?m tra c� ??i t??ng t?i v? tr� chu?n b? di chuy?n ??n v� c� th? di chuy?n t?i ?� hay kh�ng
        Entity e[] =new Entity[4];
       
        e[0]= _board.getEntity(Coordinates.pixelToTile(_x+x),Coordinates.pixelToTile(_y+y)-1, this);//kiem tra phia tren , sau lung boomber
        e[1]= _board.getEntity(Coordinates.pixelToTile(_x+_sprite.getRealWidth()+ x+2),Coordinates.pixelToTile(_y + y)-1, this);// kiem tra dau ben phai boomber
        e[2]= _board.getEntity(Coordinates.pixelToTile(_x+_sprite.getRealWidth()+x+2),Coordinates.pixelToTile(_y+y-1), this);// kiem tra ben phai , duoi chan boomber
        e[3]= _board.getEntity(Coordinates.pixelToTile(_x+x),Coordinates.pixelToTile(_y+y-1), this);// kiem tra sau lung duoi chan boomber
  
        for(int i=0;i<4;i++){
      
            if(!e[i].collide(this)){
                return false;
            }
        }
        return true;
    }

    @Override
    /**
     * Di chuyen boomber
     */
    public void move(double xa, double ya) {
        // TODO: s? d?ng canMove() ?? ki?m tra xem c� th? di chuy?n t?i ?i?m ?� t�nh to�n hay kh�ng v� th?c hi?n thay ??i t?a ?? _x, _y
        // TODO: nh? c?p nh?t gi� tr? _direction sau khi di chuy?n
                if(xa > 0) _direction = 1; // ben phai 
		if(xa < 0) _direction = 3;// ben trai
		if(ya > 0) _direction = 2;// ben duoi
		if(ya < 0) _direction = 0;// ben tren 
		
		if(canMove(0, ya)) { //separate the moves for the player can slide when is colliding
			_y += ya;// di chuyen len, xuong 
		}
		
		if(canMove(xa, 0)) {
			_x += xa;// di chuyen phai trai
		}
                
    }

    @Override
     /**
         * La ham kiem tra va tram cua Item voi Enity
         * @param e Enity truyen vao
         * @return boolean la kieu tra ve
         */
    public boolean collide(Entity e) {
        // TODO: x? l� va ch?m v?i Flame
        // TODO: x? l� va ch?m v?i Enemy
        if(e instanceof Flame) {
            String filename = "PLAYER_OUT.wav";
            ContinuousAudioDataStream loop = null;
            InputStream in = null;
            try {
                in = new FileInputStream(filename);
            } catch (FileNotFoundException ex) {
                System.out.println("File not found");
            }
            try {
                AudioStream s = new AudioStream(in);
                AudioData MD;
                AudioPlayer.player.start(s);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
			kill();
			return false;
		}
		
		if(e instanceof Enemy) {
			kill();
			return true;
		}
		
		return true;
       
    }

    private void chooseSprite() {
        switch (_direction) {
            case 0:
                _sprite = Sprite.player_up;
                if (_moving) {
                    _sprite = Sprite.movingSprite(Sprite.player_up_1, Sprite.player_up_2, _animate, 20);
                }
                break;
            case 1:
                _sprite = Sprite.player_right;
                if (_moving) {
                    _sprite = Sprite.movingSprite(Sprite.player_right_1, Sprite.player_right_2, _animate, 20);
                }
                break;
            case 2:
                _sprite = Sprite.player_down;
                if (_moving) {
                    _sprite = Sprite.movingSprite(Sprite.player_down_1, Sprite.player_down_2, _animate, 20);
                }
                break;
            case 3:
                _sprite = Sprite.player_left;
                if (_moving) {
                    _sprite = Sprite.movingSprite(Sprite.player_left_1, Sprite.player_left_2, _animate, 20);
                }
                break;
            default:
                _sprite = Sprite.player_right;
                if (_moving) {
                    _sprite = Sprite.movingSprite(Sprite.player_right_1, Sprite.player_right_2, _animate, 20);
                }
                break;
        }
    }
    /**
     * Them Item cho boomber
     * @param p  doi tuong kieu item truyen vao
     * @return khong co kieu tra ve 
     */
   public void addPowerup(Item p) {
		if(p.isRemoved()) return;
		
		_Item.add(p);
		
		p.setValues();
                // System.out.println("in");
	}
/**
 * Xoa bo Item xong khi da duoc collide
 * @return  khong co kieu tra ve
 */
	public void clearUsedPowerups() {
		Item p;
		for (int i = 0; i < _Item.size(); i++) {
			p = _Item.get(i);
			if(p.isActive() == false)
				_Item.remove(i);
		}
	}
/**
 * Xoa bo het cau Item 
 */
	public void removePowerups() {
		for (int i = 0; i < _Item.size(); i++) {
				_Item.remove(i);
		}
	}
	
	
}
