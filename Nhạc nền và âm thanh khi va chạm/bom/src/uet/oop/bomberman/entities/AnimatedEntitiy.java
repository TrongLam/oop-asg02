package uet.oop.bomberman.entities;

/**
 * Class cho AnimatedEntitiy cua game hoat dong 
 * @author Nguyen Trong Lam & Pham Thanh Hung
 * @version 1.0
 * @Since 12/2/2018
 * 
 */
public abstract class AnimatedEntitiy extends Entity {

	protected int _animate = 0;
	protected final int MAX_ANIMATE = 7500;
	
	protected void animate() {
		if(_animate < MAX_ANIMATE) _animate++; else _animate = 0;
	}

}
