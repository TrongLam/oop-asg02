package uet.oop.bomberman;


import sun.audio.*;

import java.io.*;
public class Sound {
	
	public static void music(){
		AudioPlayer MGP = AudioPlayer.player;
		AudioStream BGM;
		AudioData MD;
		ContinuousAudioDataStream loop = null;
		try{
			BGM = new AudioStream(new FileInputStream("res\\Sounds\\487135_RR-Bomberman-Theme.mp3"));
			MD = BGM.getData();
			loop = new ContinuousAudioDataStream(MD);
		}catch(IOException error){
			System.out.print("file not found");
		}
		
		MGP.start(loop);
	}
        public static void main(String[] args) {
            music();
        }
}
